//
//  IssueListVC.swift
//  Assignment
//
//  Created by Shezad Ahamed on 01/11/21.
//

import UIKit

class IssueListVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var issueList: [ProjectIssueViewModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()
        
        apiCall()
    }
    
    func apiCall(){
        APIService().load(resource: ProjectIssues.get) { [weak self] result in
            
            guard let strongSelf = self else { return }
            
            switch result {
            case .success(let issueData):
                
                strongSelf.issueList = issueData.map(ProjectIssueViewModel.init)
                strongSelf.tableView.reloadData()
                
            case .failure(let error):
                print(error)
            }
        }
        
    }
}

extension IssueListVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        issueList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "IssueTVCell", for: indexPath) as? IssueTVCell{
            cell.issueData = issueList[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let issue = issueList[indexPath.row]
        
        if issue.comments == 0{
            let alert = UIAlertController(title: "", message: "There are no comments for selected issue", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { (_) in
                 }))
            self.present(alert, animated: true, completion: nil)
        }else{
            if let detailVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CommentsVC") as? CommentsVC{
                detailVC.commentUrl = issue.commentsUrl
                self.show(detailVC, sender: self)
            }
        }
    }
}
