//
//  CommentsVC.swift
//  Assignment
//
//  Created by Shezad Ahamed on 02/11/21.
//

import UIKit

class CommentsVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var commentList: [IssueCommentsViewModel] = []
    var commentUrl: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Comments"

        tableView.tableFooterView = UIView()
        
        apiCall()
    }
    
    func apiCall(){
        APIService().load(resource: IssueComments.get(urlString: commentUrl)) { [weak self] result in
            
            guard let strongSelf = self else { return }
            
            switch result {
            case .success(let commentData):
                
                strongSelf.commentList = commentData.map(IssueCommentsViewModel.init)
                strongSelf.tableView.reloadData()
                
            case .failure(let error):
                print(error)
            }
        }
    }
}

extension CommentsVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier:"CommentsTVCell", for: indexPath) as? CommentsTVCell{
            cell.commentData = commentList[indexPath.row]
            return  cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
}
