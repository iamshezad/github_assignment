//
//  ProjectIssueViewModel.swift
//  Assignment
//
//  Created by Shezad Ahamed on 01/11/21.
//


import Foundation
import UIKit

struct ProjectIssueViewModel {
    
    let title: String
    let body: String
    let comments: Int
    let commentsUrl: String
    let status: IssueStatus
    let statusColor: UIColor
   
    init(model:ProjectIssue){
        title = model.title ?? ""
        body = model.body ?? ""
        comments = model.comments ?? 0
        commentsUrl = model.commentsUrl ?? ""
        if model.state ?? "" == "open"{
            status = .open
            statusColor = .green
        }else{
            status = .closed
            statusColor = .magenta
        }
    }

}
        

enum IssueStatus {
    case open
    case closed
}
