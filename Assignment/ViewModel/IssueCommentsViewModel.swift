//
//  IssueCommentsViewModel.swift
//  Assignment
//
//  Created by Shezad Ahamed on 02/11/21.
//

import Foundation
import UIKit

struct IssueCommentsViewModel {
    
    let name: String
    let body: String
    let imageUrl: String
    
    
    init(model:IssueComment){
        body = model.body ?? ""
        
        if let user = model.user{
            name = user.login ?? ""
            imageUrl = user.avatarUrl ?? ""
        }else{
            name = ""
            imageUrl = ""
        }
        
    }
    
}
