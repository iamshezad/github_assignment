//
//  IssueTVCell.swift
//  Assignment
//
//  Created by Shezad Ahamed on 01/11/21.
//

import UIKit

class IssueTVCell: UITableViewCell {

    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var issueData: ProjectIssueViewModel?{
        didSet{
            guard let data = issueData else { return }
            statusView.backgroundColor = data.statusColor
            titleLabel.text = data.title
            descriptionLabel.text = data.body
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()

        statusView.layer.cornerRadius = 10
        statusView.layer.masksToBounds = true
        
    }
    
}
