//
//  CommentsTVCell.swift
//  Assignment
//
//  Created by Shezad Ahamed on 01/11/21.
//

import UIKit

class CommentsTVCell: UITableViewCell {
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userComment: UILabel!
    
    var commentData: IssueCommentsViewModel?{
        didSet{
            guard let data = commentData else { return }
            
            userNameLabel.text = data.name
            userComment.text = data.body
            
            if let url = URL(string:data.imageUrl){
                userImageView.load(url: url)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        userImageView.layer.cornerRadius = 25
        userImageView.layer.masksToBounds = true
    }
}
