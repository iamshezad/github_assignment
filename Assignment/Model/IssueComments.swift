//
//  IssueComments.swift
//  Assignment
//
//  Created by Shezad Ahamed on 02/11/21.
//

import Foundation

// MARK: - IssueComment
struct IssueComment: Codable {
    let url: String?
    let htmlUrl: String?
    let issueUrl: String?
    let id: Int?
    let nodeId: String?
    let user: User?
    let createdAt: String?
    let updatedAt: String?
    let authorAssociation: String?
    let body: String?
    let reactions: Reactions?

    enum CodingKeys: String, CodingKey {
        case url = "url"
        case htmlUrl = "html_url"
        case issueUrl = "issue_url"
        case id = "id"
        case nodeId = "node_id"
        case user = "user"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case authorAssociation = "author_association"
        case body = "body"
        case reactions = "reactions"
    }
}

typealias IssueComments = [IssueComment]


extension IssueComments{
    static func get(urlString: String) -> Resource<IssueComments> {
        
        guard let url = URL(string: urlString) else {
            fatalError("URL is incorrect!")
        }
        
        return Resource<IssueComments>(url: url)
    
    }
}
