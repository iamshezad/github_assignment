//
//  ProjectIssue.swift
//  Assignment
//
//  Created by Shezad Ahamed on 01/11/21.
//

import Foundation

// MARK: - ProjectIssue
struct ProjectIssue: Codable {
    let url: String?
    let repositoryUrl: String?
    let labelsUrl: String?
    let commentsUrl: String?
    let eventsUrl: String?
    let htmlUrl: String?
    let id: Int?
    let nodeId: String?
    let number: Int?
    let title: String?
    let user: User?
    let labels: [Label]?
    let state: String?
    let locked: Bool?
    let assignee: User?
    let assignees: [User]?
    let comments: Int?
    let createdAt: String?
    let updatedAt: String?
    let authorAssociation: String?
    let pullRequest: PullRequest?
    let body: String?
    let reactions: Reactions?
    let timelineUrl: String?

    enum CodingKeys: String, CodingKey {
        case url = "url"
        case repositoryUrl = "repository_url"
        case labelsUrl = "labels_url"
        case commentsUrl = "comments_url"
        case eventsUrl = "events_url"
        case htmlUrl = "html_url"
        case id = "id"
        case nodeId = "node_id"
        case number = "number"
        case title = "title"
        case user = "user"
        case labels = "labels"
        case state = "state"
        case locked = "locked"
        case assignee = "assignee"
        case assignees = "assignees"
        case comments = "comments"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case authorAssociation = "author_association"
        case pullRequest = "pull_request"
        case body = "body"
        case reactions = "reactions"
        case timelineUrl = "timeline_url"
    }
}

// MARK: - User
struct User: Codable {
    let login: String?
    let id: Int?
    let nodeId: String?
    let avatarUrl: String?
    let gravatarId: String?
    let url: String?
    let htmlUrl: String?
    let followersUrl: String?
    let followingUrl: String?
    let gistsUrl: String?
    let starredUrl: String?
    let subscriptionsUrl: String?
    let organizationsUrl: String?
    let reposUrl: String?
    let eventsUrl: String?
    let receivedEventsUrl: String?
    let type: String?
    let siteAdmin: Bool?

    enum CodingKeys: String, CodingKey {
        case login = "login"
        case id = "id"
        case nodeId = "node_id"
        case avatarUrl = "avatar_url"
        case gravatarId = "gravatar_id"
        case url = "url"
        case htmlUrl = "html_url"
        case followersUrl = "followers_url"
        case followingUrl = "following_url"
        case gistsUrl = "gists_url"
        case starredUrl = "starred_url"
        case subscriptionsUrl = "subscriptions_url"
        case organizationsUrl = "organizations_url"
        case reposUrl = "repos_url"
        case eventsUrl = "events_url"
        case receivedEventsUrl = "received_events_url"
        case type = "type"
        case siteAdmin = "site_admin"
    }
}

// MARK: - Label
struct Label: Codable {
    let id: Int?
    let nodeId: String?
    let url: String?
    let name: String?
    let color: String?
    let labelDefault: Bool?
    let labelDescription: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case nodeId = "node_id"
        case url = "url"
        case name = "name"
        case color = "color"
        case labelDefault = "default"
        case labelDescription = "description"
    }
}

// MARK: - PullRequest
struct PullRequest: Codable {
    let url: String?
    let htmlUrl: String?
    let diffUrl: String?
    let patchUrl: String?

    enum CodingKeys: String, CodingKey {
        case url = "url"
        case htmlUrl = "html_url"
        case diffUrl = "diff_url"
        case patchUrl = "patch_url"
    }
}

// MARK: - Reactions
struct Reactions: Codable {
    let url: String?
    let totalCount: Int?
    let the1: Int?
    let reactions1: Int?
    let laugh: Int?
    let hooray: Int?
    let confused: Int?
    let heart: Int?
    let rocket: Int?
    let eyes: Int?

    enum CodingKeys: String, CodingKey {
        case url = "url"
        case totalCount = "total_count"
        case the1 = "+1"
        case reactions1 = "-1"
        case laugh = "laugh"
        case hooray = "hooray"
        case confused = "confused"
        case heart = "heart"
        case rocket = "rocket"
        case eyes = "eyes"
    }
}

typealias ProjectIssues = [ProjectIssue]

extension ProjectIssues{
    static var get: Resource<ProjectIssues> = {
        
        guard let url = URL(string: "https://api.github.com/repos/firebase/firebase-ios-sdk/issues") else {
            fatalError("URL is incorrect!")
        }
        
        return Resource<ProjectIssues>(url: url)
        
    }()
}
